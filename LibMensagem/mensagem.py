#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import datetime, json

class Mensagem(object):

    def __init__(self, nomeCliente: str, contador: int):
        self.__objetoMensagem = \
                {
                    "contador": contador,
                    "nomeCliente": nomeCliente
                };

    def __init__(self, stringMensagemJson: str, origem: str):
        self.__objetoMensagem = json.loads(stringMensagemJson.decode("utf-8"));
        self.__objetoMensagem["origem"] = origem;
        self.__objetoMensagem["dataHoraRecepcaoServer"] = \
            datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S");
    
    def getMensagem(self):
        return self.__objetoMensagem;

    def getContador(self):
        return self.__objetoMensagem["contador"];

    def gerarJsonMensagemEnvio(self, conteudo: str):
        self.__objetoMensagem["dataHoraEnvio"] = \
            datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S");
        return json.dumps(self.__objetoMensagem);

    def gerarJsonMensagemResposta(self, conteudo: str):
        mensagemResposta = self.__gerarMensagem(conteudo);
        #"Resposta da mensagem " + str(self.__stringMensagemJson['contador'])
        return json.dumps(mensagemResposta);

    def __gerarMensagem(self, conteudo: str):
        self.__objetoMensagem["mensagem"] = conteudo;
        return self.__objetoMensagem;