#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import sys, os, threading, datetime, json, requests
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + 
                os.path.sep + '..');

from LibMensagem.mensagem import Mensagem

HTTPS_SERVER_URL = 'http://127.0.0.1:5000/';

MENSAGEM_ERRO_PARAMETROS = "clientHttps.py CLIENT_NAME TOPIC_TO_PUBLISH NUMBER_MESSAGES_TO_SEND";
diretorioLog = os.path.dirname(os.path.abspath(__file__)) + os.path.sep + "logs";
if not (os.path.exists(diretorioLog)):
    os.mkdir(diretorioLog);

if len(sys.argv) > 3:
    nomeCliente = sys.argv[1];
    #recupera o topico para publiar
    topicoParaPublicar = sys.argv[2];
    quantidadeMensagens = sys.argv[3];
else:
    raise Exception(MENSAGEM_ERRO_PARAMETROS);

def publicador(nomeCliente, topicosParaPublicar, qtdMensagens):
    for contador in range(1, int(qtdMensagens)):
        mensagem =  Mensagem(nomeCliente=nomeCliente, contador=contador);
        publicarMensagem(contador, topicosParaPublicar, mensagem);

def publicarMensagem(id_mensagem, topicosParaPublicar, mensagem):
    response = requests.post(HTTPS_SERVER_URL+ topicosParaPublicar, 
                    mensagem.gerarJsonMensagemEnvio(conteudo="1234"));
    if response.status_code != 200:
        print("Erro no envio da mensagem {0}".format(id_mensagem));      

def main():
    theadPublicador = threading.Thread(target=publicador, 
                                        args=( 
                                                nomeCliente,
                                                topicoParaPublicar, 
                                                quantidadeMensagens
                                            )
                                    );
    theadPublicador.daemon = True;
    theadPublicador.start();

    # Permanece em loop mesmo se houver erros. 
    while True:
        try:
            theadPublicador.join();
        except KeyboardInterrupt: 
            print("saindo");
            break;

if __name__ == '__main__':
    main();