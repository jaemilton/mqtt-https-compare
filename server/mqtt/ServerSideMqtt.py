#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import sys, os, threading, datetime, json
import paho.mqtt.client as mqtt

sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + 
                os.path.sep + '..' + os.path.sep + '..' + os.path.sep +
                'LibMensagem');

from mensagem import Mensagem
import logging

topicosParaAssinar = [
    ('TOPICO_01', 0),
    ('TOPICO_02', 0),
    ('TOPICO_03', 0),
    ('TOPICO_04', 0),
    ('TOPICO_05', 0),
    ('TOPICO_06', 0),
    ('TOPICO_07', 0),
    ('TOPICO_08', 0),
    ('TOPICO_09', 0),
    ('TOPICO_10', 0)
]
diretorioLog = os.path.dirname(os.path.abspath(__file__)) + os.path.sep + "logs";
if not (os.path.exists(diretorioLog)):
    os.mkdir(diretorioLog);

# Mosquitto MQTT
# username 	= "admin"
# password 	= "12345"
hostname 	= "mqtt-server"
port 		= 8883

# Funções de Callback dos eventos
# on_connect = função chamada quando ocorre a conexão entre o cliente e o broker MQTT.
# on_message = função chamada quando uma mensagem de um tópico assinado for recebido.
# on_publish = função chamada quando uma mensagem for publicada. 
# on_subscribe = função chamada quando um tópico for assinado pelo cliente MQTT.
# on_log = função para debug do Paho.

def on_connect(client, userdata, flags, rc):
    print("rc: " + str(rc))

def on_message(client, obj, msg):
    # Imprime o conteudo da messagem.
    logging.debug(json.dumps(msg.payload));
    mensagem = Mensagem(msg.payload,  msg.topic);
    #Responde cada mensagem para o topico de reposta
    topicoResposta = msg.topic + '_R';
    publicarMensagem(
        client, 
        topicoResposta, 
        mensagem.getContador, 
        mensagem.gerarJsonMensagemResposta(
            conteudo = "Resposta da mensagem via mqtt" + str(mensagem.getContador)
        ));
    # print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload) )

#def on_publish(client, obj, mid):
#    print("mid: " + str(mid))

def on_subscribe(client, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))

def on_log(client, obj, level, string):
    print(string)

def publicarMensagem(mqttClient, topicosParaPublicar, id_mensagem, mensagemJson):
    rc = mqttClient.publish(topicosParaPublicar, mensagemJson);
    if not (rc.rc == mqtt.MQTT_ERR_SUCCESS):
        print("Erro no envio da mensagem {0}".format(id_mensagem));


#------------------------------------------------

def main():
    logging.basicConfig(filename="{0}{1}{2}_{3}.log".format(diretorioLog, 
                                os.path.sep,
                                'ServerSideMqtt',
                                datetime.datetime.now().strftime("%Y%m%d%H%M%S")),
                        level=logging.DEBUG, 
                        #format='%(asctime)s;%(message)s',
                        format='%(message)s',
                        #datefmt='%d/%m/%Y %H:%M:%S',
                        filemode='w');
    
    mqttClient = mqtt.Client();

    # Assina as funções de callback
    mqttClient.on_message = on_message
    mqttClient.on_connect = on_connect
    #mqttClient.on_publish = on_publish
    mqttClient.on_subscribe = on_subscribe

    # Uncomment to enable debug messages
    #mqttClient.on_log = on_log

    # Connecta ao Mosquitto MQTT. 
    # Informe o nome do usuário e senha. 
    # mqttClient.username_pw_set(username, password)

    # Carrega o certificado TLS.
    mqttClient.tls_set(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".." + os.path.sep + "mqtt-ca.crt")

    # Informe o endereço IP ou DNS do servidor Mosquitto. 
    # Use 1883 para conexão SEM TLS, e 8883 para conexão segura TLS.
    mqttClient.connect(hostname, port);

    # Assina o tópico, com QoS level 0 (pode ser 0, 1 ou 2)
    #mqttClient.subscribe("#", 0)
    mqttClient.subscribe(topicosParaAssinar)
    
    # Para publicar em um tópico, utilize a função a seguir.
    # mqttClient.publish(topic_publish, "25")


    # Permanece em loop mesmo se houver erros. 
    while True:
        try:
            rc = 0
            while rc == 0:
                rc = mqttClient.loop()
            print("rc: " + str(rc))
        except KeyboardInterrupt: 
            print("saindo");
            break;

if __name__ == '__main__':
    main();