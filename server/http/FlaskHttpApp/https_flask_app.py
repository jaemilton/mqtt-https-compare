#!/usr/bin/python3
# -*- coding: UTF-8 -*-
#
# Conteudo do arquivo `myapp.py`
#
from flask import Flask
from flask import request
import os, sys, json

sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + 
                os.path.sep + '..' + os.path.sep + '..' + os.path.sep + '..');

from LibMensagem.mensagem import Mensagem

app = Flask(__name__)


@app.route("/")
def hello():
    return "Servico no AR";

@app.route('/mensagem/<nomeTopico>', methods=['POST']) #GET requests will be blocked
def json_example(nomeTopico):
    #origem = request.path.split('/')[-1]; #Recupera a parte final da url
    mensagem =  Mensagem(request.data, nomeTopico); 
    return mensagem.gerarJsonMensagemResposta(
            conteudo = "Resposta da mensagem {} via https".format(mensagem.getContador())
        );

if __name__ == "__main__":
    app.run(host='0.0.0.0')