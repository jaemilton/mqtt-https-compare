#!/usr/bin/python
import subprocess, sys
## command to run - tcp only ##
#cmd = "/usr/sbin/nethogs -v 2 -d 5 -t | grep mosquitto"
cmd = "/usr/sbin/nethogs -v 2 -d 5 -t"

## run it ##
process = subprocess.Popen(cmd,
			   shell=True,
                           stdout=subprocess.PIPE,
                           universal_newlines=True)
while True:
    output = process.stdout.readline()
    #print(output.strip())
    # Do something else
    return_code = process.poll()
    if return_code is not None:
        print('RETURN CODE', return_code)
        # Process has finished, read rest of the output 
        for output in process.stdout.readlines():
            print(output.strip())
        break
    if output != '' and output.find("mosquitto") >= 0:
        f = open("mosquito.out","w+")
	f.write(output)
	f.close()
